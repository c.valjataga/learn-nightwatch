var config = require('../../nightwatch.conf.js');

module.exports = { 
  'HLTV.org': function(browser) {
    browser
      .url('https://hltv.org/')
      .waitForElementVisible('body')
      .assert.title('CS:GO News & Coverage | HLTV.org')
      .saveScreenshot(config.imgpath(browser) + 'hltv-esileht.png')
      .assert.containsText(".navnews", "News")
      .useXpath()
      .click("//a[text()='Stats']")
      .assert.title('CS:GO Statistic Database | HLTV.org')
      .saveScreenshot(config.imgpath(browser) + 'hltv-stats.png')
      .click()
      .end();
  }
};